const player = require('play-sound')(opts = {});

class RingtonService {

    _isRinging = false;
    
    constructor() {
        this._isRinging = false;
    }
    
    setRington(){
        return true;
    }
    
    addRington(){
        console.log("adding rington");
        setRington();
    }
    
    removeRington(){
        console.log("Removing rington");
        setRington();
    }
    
    async ring(){
        if (this._isRinging){
            return;
        }
        this._isRinging = true;
        player.play('./ringtons/default.wav', (err) => {
            if (err){
                console.log(err);
            }
            this._isRinging = false;
        });
    }    
}


module.exports = new RingtonService();
