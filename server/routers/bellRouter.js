const express = require('express');
const bellRouter = express.Router();
const ringtonService = require('../services/ringtonService');

bellRouter.post("/ring", (req, res) => {
    ringtonService.ring();
    console.log("Test: ring works");
    res.status(200).send("Test: ring works");
});
/*
bellRouter.post("/motion", (req, res) => {
    console.log("Test: motion works");
    res.send("Test: motion works");
});
*/

module.exports = bellRouter;
