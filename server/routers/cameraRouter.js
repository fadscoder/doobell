const express = require('express');
const request = require('request');
const cameraRouter = express.Router();

const baseHost = 'http://192.168.4.1'; // lorsque esp32 est access point
// const baseHost = 'http://192.168.0.199';
const streamUrl = baseHost + ':81'
  
function fetchUrl(url, cb){
    fetch(url)
    .then(function (response) {
        if (response.status !== 200) {
        cb(response.status, response.statusText);
        } else {
        response.text().then(function(data){
            cb(200, data);
        }).catch(function(err) {
            cb(-1, err);
        });
        }
    })
    .catch(function(err) {
        cb(-1, err);
    });
}
  
function setReg(reg, offset, mask, value, cb){
    //console.log('Set Reg', '0x'+reg.toString(16), offset, '0x'+mask.toString(16), '0x'+value.toString(16), '('+value+')');
    value = (value & mask) << offset;
    mask = mask << offset;
    fetchUrl(`${baseHost}/reg?reg=${reg}&mask=${mask}&val=${value}`, cb);
}
  
function getReg(reg, offset, mask, cb){
    mask = mask << offset;
    fetchUrl(`${baseHost}/greg?reg=${reg}&mask=${mask}`, function(code, txt){
    let value = 0;
    if(code == 200){
        value = parseInt(txt);
        value = (value & mask) >> offset;
        txt = ''+value;
    }
    cb(code, txt);
    });
}

function setXclk(xclk, cb){
    fetchUrl(`${baseHost}/xclk?xclk=${xclk}`, cb);
}

function setWindow(start_x, start_y, end_x, end_y, offset_x, offset_y, total_x, total_y, output_x, output_y, scaling, binning, cb){
    fetchUrl(`${baseHost}/resolution?sx=${start_x}&sy=${start_y}&ex=${end_x}&ey=${end_y}&offx=${offset_x}&offy=${offset_y}&tx=${total_x}&ty=${total_y}&ox=${output_x}&oy=${output_y}&scale=${scaling}&binning=${binning}`, cb);
}

// set register
cameraRouter.patch("/:reg", (req, res) => {
    const reg = req.params.reg;
    const mask = req.body.mask;
    const value = req.body.value;
    if (!reg || !mask || !value){
        res.status(400).send("Bad request");
        return;
    }
    setReg(reg, 0, mask, value, function(code, txt){
        if(code != 200){
            res.status(500).send('Error['+code+']: ' + txt);
        }
        else{
            res.status(204).send();
        }
      });
});


cameraRouter.get("/reg/:reg/:mask/:value", (req, res) => {
    const reg = req.params.reg;
    const mask = req.params.mask;
    const value = req.params.value;
    if (!reg || !mask || !value){
        res.status(400).send("Bad request");
        return;
    }
    getReg(reg, 0, mask, value, function(code, txt){
        if(code != 200){
          res.status(500).send('Error['+code+']: ' + txt);
        }
        else {
            res.status(200).json({ reg:  '0x'+parseInt(txt).toString(16)+' ('+txt+')' });
        }
      });
});

cameraRouter.patch("/xclk", (req, res) => {
    const newValue = req.body.value;
    if (!newValue){
        res.status(400).send("Bad request");
        return;
    }
    setXclk(newValue, function(code, txt){
        if(code != 200){
          res.status(500).send('Error['+code+']: ' + txt);
        }
        else {
            res.status(204).send();
        }
    });
});

cameraRouter.patch("/resolution", (req, res) => {
    const start_x = req.body.start_x;
    const offset_x = req.body.offset_x;
    const offset_y = req.body.offset_y;
    const total_x = req.body.total_x;
    const total_y = req.body.total_y;
    const output_x = req.body.output_x;
    const output_y = req.body.output_y;
    setWindow(start_x, 0, 0, 0, offset_x, offset_y, total_x, total_y, output_x, output_y, false, false, function(code, txt){
        if(code != 200){
            res.status(500).send('Error['+code+']: ' + txt);
        }
        else {
            res.status(204).send();
        }
    });
});

cameraRouter.use("/:param", async (req, res) => {
    request.get(`${streamUrl}/stream`).pipe(res);
});

cameraRouter.use("/stream", (req, res) => {
    console.log("Test: stream workds");
    console.log(req.method);
    fetch(`${streamUrl}/stream`, {
        method: 'GET',
        headers: req.headers,
    })
    .then(esp32res => {
        if (!esp32res.ok) {
            throw new Error("HTTP status: " + response.status);
        }
        return esp32res.text();
    })
    .then(textAnswer => {
        console.log(textAnswer);
        res.send(textAnswer);
    })
    .catch(error => {
        console.log(error);
        res.status(500).json(error);
    });
});

cameraRouter.get("/capture", (req, res) => {
    fetch(`${streamUrl}/capture?_cb=${Date.now()}`, {
        method: 'GET',
        headers: req.headers,
        body: req.body
    })
    .then(esp32res => {
        if (!esp32res.ok) {
            throw new Error("HTTP status: " + response.status);
        }
        return {status: response.status, text: response.text()};
    })
    .then(answer => {
        res.status(answer.status).send(answer.text);
    })
    .catch(error => {
        console.log(error);
        res.status(500).json(error);
    });
});

module.exports = cameraRouter;
