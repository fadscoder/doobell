const express = require('express');
const cors = require('cors');
const cameraRouter = require("./routers/cameraRouter");
const bellRouter = require("./routers/bellRouter");

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use("/camera", cameraRouter);
app.use("/bell", bellRouter);

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server availbable on http://192.168.0.13:${PORT}`);
});

/*
TODO:
1- completer les requete du roouter de camera afin de pouvoir utiliser l'interface graphique comme si c'etait l'esp32
2- completer belle
3- changer interface utilisateur
4- servir interface utilisateur via serveur dynamique
5- ajouter configurations au serveur
*/
