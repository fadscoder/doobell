const electron = require('electron');
const { app, BrowserWindow } = electron;

app.on('ready', () => {
  const mainWindow = new BrowserWindow({width: 800, height: 600});
  console.log(__dirname);
  mainWindow.loadURL(`https://google.com`);
  //mainWindow.loadURL(`file://${__dirname}/index.html`);
});