# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/home/fadscoder/esp-idf/components/bootloader/subproject"
  "/home/fadscoder/projects/DoorBell/cs/build/bootloader"
  "/home/fadscoder/projects/DoorBell/cs/build/bootloader-prefix"
  "/home/fadscoder/projects/DoorBell/cs/build/bootloader-prefix/tmp"
  "/home/fadscoder/projects/DoorBell/cs/build/bootloader-prefix/src/bootloader-stamp"
  "/home/fadscoder/projects/DoorBell/cs/build/bootloader-prefix/src"
  "/home/fadscoder/projects/DoorBell/cs/build/bootloader-prefix/src/bootloader-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/home/fadscoder/projects/DoorBell/cs/build/bootloader-prefix/src/bootloader-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "/home/fadscoder/projects/DoorBell/cs/build/bootloader-prefix/src/bootloader-stamp${cfgdir}") # cfgdir has leading slash
endif()
